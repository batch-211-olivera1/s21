console.log("Hello World");

// Array is a list of data

// Multiple variable
	let studentNumberA = "2020-1923";
	let studentNumberB = "2020-1924";
	let studentNumberC = "2020-1925";
	let studentNumberD = "2020-1926";
	let studentNumberE = "2020-1927";
	
// Array
/*
	-Store multiple values in a single variable
	-declared using ([]) also known as "Array Literals"
	
	Syntax:
		let/const arrayName = [elementA,elementB,elementC,elementD...]


*/

// Example

let studentNumbers = ["2020-1923","2020-1924","2020-1925","2020-1926","2020-1927"];

let grades = [98.5, 94.3,89.2, 90.1];
let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu"];

console.log(grades);
console.log(computerBrands);

// Possible use of an array but it is not recommended

let mixedArr = [12, "Asus", null, undefined,{}];
console.log(mixedArr);

// Alternative way to write arrays
let myTask = [
	"drink html",
	"eat javascript",
	"inhale css",
	"bake sass"
]
console.log(myTask);

let task = ["sleep", "eat", "Drink", "Ride", "repeat"];
let capitalCities = ["Tokyo", "Shanghai", "Sao Paulo", "London",];
console.log(task);
console.log(capitalCities);


let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1,city2,city3];
console.log(cities);

// .length property
/*
	-allows us to get and set the total number of items in an array.
	-some array methods and properties can also be used with strings
	-we can set the total number of items in an array. delete the last item in the array or shorten the array by simply updating the length property of an array
*/

console.log(myTask.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr);

let fullName = "Jamie Noble";
console.log(fullName.length);

myTask.length = myTask.length-1;
console.log(myTask.length);
console.log(myTask);

// decrementation
cities.length--;
console.log(cities);

fullName.length = fullName.length-1;
console.log(fullName.length);
fullName.length--;
console.log(fullName);

let theBeatles = ["John", "Paul", "Ringo", "George"];
// theBeatles.length++;
theBeatles[4]="Cardo";
theBeatles[theBeatles.length]="Ely";
theBeatles[theBeatles.length]="Chito";
theBeatles[theBeatles.length]="MJ";
console.log(theBeatles);


let theTrainers = ["Ash"];

function addTrainers(trainer) {
	theTrainers[theTrainers.length]= trainer;
}
addTrainers("Misty");
addTrainers("Brock");
console.log(theTrainers);

// Reading from Arrays
/*
	-Accessing array elements is one of the more common task taht we do with an array
	-This can be done through the use of array indexes
	-each element in an array is associated with its own index/number

	Syntax
		ArrayName[index];
*/

console.log(grades[0]);
console.log(computerBrands[3]);
// undefined array that is not existing
console.log(grades[20]);

let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];

console.log(lakersLegends[1]);
console.log(lakersLegends[3]);

// Storing items in abother variable
let currentLakers = lakersLegends[2];
console.log(currentLakers);

console.log("Array before reassignment");
console.log(lakersLegends);
lakersLegends[2]="Pau Gasol";
console.log("Array after reassignment");
console.log(lakersLegends);

function findBlackMamba(index){
	return lakersLegends[index];

}

let blackMamba = findBlackMamba(0);
console.log(blackMamba);


let favoriteFoods = [
	"Tonkatsu",
	"Adobo",
	"Hamburger",
	"Sinigang",
	"Pizza"
]
console.log(favoriteFoods);

favoriteFoods[3]="Crab";
favoriteFoods[4]="Chicken";
console.log(favoriteFoods);

// Accessing the last element of an array

let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];

let lastElementIndex = bullsLegends.length-1;
console.log(lastElementIndex);
console.log(bullsLegends.length);

console.log(bullsLegends[lastElementIndex]);
console.log(bullsLegends[bullsLegends.length-1]);

// Adding items into the array

let newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);

console.log(newArr[1]);
newArr[1] = "Tifa Lockhart";
console.log(newArr);

// newArr[newArr.length-1] = "Aerith Gainsborough";

newArr[newArr.length] = "Barret Wallace";
console.log(newArr);

// Looping over an array

for(let index = 0; index < newArr.length; index++){
	console.log(newArr[index]);
}

let numArr = [5,12,30,46,40]

for(let index = 0; index < numArr.length; index++){
	if(numArr[index] % 5 === 0){
		console.log(numArr[index] + " is devisible by 5");
	}else{
		console.log(numArr[index] + " is not divisible by 5");
	}
}


// Multidimensional Array

/*
	-useful for storing complex data structures
	-a practical application of this is to help visualize/create real world object
	

*/

let chessBoard = [
	["a1", "b1", "c1", "d1","e1", "f1", "g1", "h1"],
	["a2", "b2", "c2", "d2","e2", "f2", "g2", "h2"],
	["a3", "b3", "c3", "d3","e3", "f3", "g3", "h3"],
	["a4", "b4", "c4", "d4","e4", "f4", "g4", "h4"],
	["a5", "b5", "c5", "d5","e5", "f5", "g5", "h5"],
	["a6", "b6", "c6", "d6","e6", "f6", "g6", "h6"],
	["a7", "b7", "c7", "d7","e7", "f7", "g7", "h7"],
	["a8", "b8", "c8", "d8","e8", "f8", "g8", "h8"]
];
console.log(chessBoard);

console.log(chessBoard[1][4]);
console.log("Pawn moves to: " + chessBoard[1][5])

console.log(chessBoard[7][0]);
console.log(chessBoard[5][7]);